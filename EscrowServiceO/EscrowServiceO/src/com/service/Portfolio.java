package com.service;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
@XmlRootElement(name = "Portfolio") 
public class Portfolio implements Serializable {  
	   private static final long serialVersionUID = 1L;
	 public Portfolio(){};
 public String getT1() {
		return T1;
	}
	public void setT1(String t1) {
		T1 = t1;
	}
	public String getHOLDN() {
		return HOLDN;
	}
	public void setHOLDN(String hOLDN) {
		HOLDN = hOLDN;
	}
	public String getQTY() {
		return QTY;
	}
	public void setQTY(String qTY) {
		QTY = qTY;
	}

 public Portfolio(String t1, String hOLDN, String qTY) {
	super();
	T1 = t1;
	HOLDN = hOLDN;
	QTY = qTY;
}
 private String T1;
private String HOLDN;
 private String QTY;
 
}
