package com.service;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
@XmlRootElement(name = "ESCROWCLIENTS") 
public class ESCROW_CLIENTS implements Serializable {  
	   private static final long serialVersionUID = 1L; 
	   public ESCROW_CLIENTS() {}
	   
	public ESCROW_CLIENTS(String mEMBER_CODE, String mEMBER_TYPE, String cLIENT_PREFIX, String cLIENT_SUFFIX,
			String cLIENT_TYPE, String sURNAME, String oTHER_NAMES, String dATE_OF_REGISTRATION, String dATE_CHANGED,String tel,String eml,String Address,String Bank,String Branch,String Account) {
		super();
		MEMBER_CODE = mEMBER_CODE;
		MEMBER_TYPE = mEMBER_TYPE;
		CLIENT_PREFIX = cLIENT_PREFIX;
		CLIENT_SUFFIX = cLIENT_SUFFIX;
		CLIENT_TYPE = cLIENT_TYPE;
		SURNAME = sURNAME;
		OTHER_NAMES = oTHER_NAMES;
		DATE_OF_REGISTRATION = dATE_OF_REGISTRATION;
		DATE_CHANGED = dATE_CHANGED;
		E_MAIL=eml;
		TELEPHONE=tel;
		BANK=Bank;
		BRANCH=Branch;
		ADDRESS=Address;
		ACCOUNT=Account;
	}	

	public String getMEMBER_CODE() {
		return MEMBER_CODE;
	}
	 @XmlElement 
	public void setMEMBER_CODE(String mEMBER_CODE) {
		MEMBER_CODE = mEMBER_CODE;
	}
	public String getMEMBER_TYPE() {
		return MEMBER_TYPE;
	}
	 @XmlElement 
	public void setMEMBER_TYPE(String mEMBER_TYPE) {
		MEMBER_TYPE = mEMBER_TYPE;
	}
	public String getCLIENT_PREFIX() {
		return CLIENT_PREFIX;
	}
	 @XmlElement 
	public void setCLIENT_PREFIX(String cLIENT_PREFIX) {
		CLIENT_PREFIX = cLIENT_PREFIX;
	}
	public String getCLIENT_SUFFIX() {
		return CLIENT_SUFFIX;
	}
	 @XmlElement 
	public void setCLIENT_SUFFIX(String cLIENT_SUFFIX) {
		CLIENT_SUFFIX = cLIENT_SUFFIX;
	}
	public String getCLIENT_TYPE() {
		return CLIENT_TYPE;
	}
	 @XmlElement 
	public void setCLIENT_TYPE(String cLIENT_TYPE) {
		CLIENT_TYPE = cLIENT_TYPE;
	}
	public String getSURNAME() {
		return SURNAME;
	}
	 @XmlElement 
	public void setSURNAME(String sURNAME) {
		SURNAME = sURNAME;
	}
	public String getOTHER_NAMES() {
		return OTHER_NAMES;
	}
	 @XmlElement 
	public void setOTHER_NAMES(String oTHER_NAMES) {
		OTHER_NAMES = oTHER_NAMES;
	}
	public String getDATE_OF_REGISTRATION() {
		return DATE_OF_REGISTRATION;
	}
	 @XmlElement 
	public void setDATE_OF_REGISTRATION(String dATE_OF_REGISTRATION) {
		DATE_OF_REGISTRATION = dATE_OF_REGISTRATION;
	}
	public String getDATE_CHANGED() {
		return DATE_CHANGED;
	}
	 @XmlElement 
	public void setDATE_CHANGED(String dATE_CHANGED) {
		DATE_CHANGED = dATE_CHANGED;
	}
	private String MEMBER_CODE;
	private String MEMBER_TYPE;
	private String CLIENT_PREFIX; 
	private String CLIENT_SUFFIX; 
	private String CLIENT_TYPE;
	private String SURNAME;
	private String OTHER_NAMES;
	private String DATE_OF_REGISTRATION;
	private String DATE_CHANGED;
	private String TELEPHONE;
	private String EMAIL;
	public String getADDRESS() {
		return ADDRESS;
	}

	public void setADDRESS(String aDDRESS) {
		ADDRESS = aDDRESS;
	}

	public String getBANK() {
		return BANK;
	}

	public void setBANK(String bANK) {
		BANK = bANK;
	}

	public String getBRANCH() {
		return BRANCH;
	}

	public void setBRANCH(String bRANCH) {
		BRANCH = bRANCH;
	}

	public String getACCOUNT() {
		return ACCOUNT;
	}

	public void setACCOUNT(String aCCOUNT) {
		ACCOUNT = aCCOUNT;
	}
	private String ADDRESS;
	private String BANK;
	private String BRANCH;
	private String ACCOUNT;
	public String getEMAIL() {
		return EMAIL;
	}

	public void setEMAIL(String eMAIL) {
		EMAIL = eMAIL;
	}

	public String getTELEPHONE() {
		return TELEPHONE;
	}

	public void setTELEPHONE(String tELEPHONE) {
		TELEPHONE = tELEPHONE;
	}

	public String getE_MAIL() {
		return E_MAIL;
	}

	public void setE_MAIL(String e_MAIL) {
		E_MAIL = e_MAIL;
	}
	private String E_MAIL;
	
	
}
