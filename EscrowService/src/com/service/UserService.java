package com.service;

import java.util.List; 
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces; 
import javax.ws.rs.core.MediaType;  
@Path("/UserService") 

public class UserService {  
   UserDAO userDao = new UserDAO();   
   @GET 
   @Path("/test/{id}/{id2}") 
   @Produces(MediaType.TEXT_HTML) 
   public String test(@PathParam("id") String id,@PathParam("id2") String id2){ 
      return "You requested "+ id + " marry " + id2; 
   }  
   
   @GET 
   @Path("/users") 
   @Produces(MediaType.APPLICATION_XML) 
   public List<User> getUsers(){ 
      return userDao.getAllUsers(); 
   }  
 
   @GET 
   @Path("/authin") 
   @Produces(MediaType.WILDCARD) 
   public String getauthin(){ 
      return userDao.getSAuth(); 
   }  
 
   @GET 
   @Path("/clivetoken") 
   @Produces(MediaType.WILDCARD) 
   public String getcauthin(){ 
      return userDao.getCAuth(); 
   }  
   
   @POST
   @Path("/simulate/{s}/{c}/{a}/{m}/{b}") 
   @Produces(MediaType.WILDCARD) 
   public String test(@PathParam("s") String s,@PathParam("c") String c,@PathParam("a") String a,@PathParam("m") String m,@PathParam("b") String b){ 
      return userDao.getApp(s,c,a,m,b);
   }
  
   @POST
   @Path("/simulatec/{i}/{c}/{a}/{pa}/{pb}/{rem}") 
   @Produces(MediaType.WILDCARD) 
   public String testN(@PathParam("i") String i,@PathParam("c") String c,@PathParam("a") String a,@PathParam("pa") String pa,@PathParam("pb") String pb,@PathParam("rem") String rem){ 
      return userDao.getAppC(i,c,a,pa,pb,rem);
   }
}
