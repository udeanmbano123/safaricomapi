package com.service;
import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
@XmlRootElement(name = "ISSUERLIST") 
public class IssuerList implements Serializable {  
	   private static final long serialVersionUID = 1L;
	   
	   public IssuerList() {}
		public String getNAME() {
		return NAME;
	}
	public void setNAME(String nAME) {
		NAME = nAME;
	}
	public String getISSUER_CODE() {
		return ISSUER_CODE;
	}
	public void setISSUER_CODE(String iSSUER_CODE) {
		ISSUER_CODE = iSSUER_CODE;
	}
		private String NAME;
		public IssuerList(String nAME, String iSSUER_CODE) {
			super();
			NAME = nAME;
			ISSUER_CODE = iSSUER_CODE;
		}
		private String ISSUER_CODE;
}
