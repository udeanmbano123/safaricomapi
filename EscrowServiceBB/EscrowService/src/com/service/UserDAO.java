package com.service;

import java.io.File; 
import java.io.FileInputStream; 
import java.io.FileNotFoundException;  
import java.io.FileOutputStream; 
import java.io.IOException; 
import java.io.ObjectInputStream; 
import java.io.ObjectOutputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;  
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Properties;
import java.util.TimeZone;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class UserDAO {  
	
        
   public List<User> getAllUsers(){ 
      
      List<User> userList =new ArrayList<User>(); 
    userList.add(new User(1, "Udean Mbano", "Java Developer"));
      userList.add(new User(2, "Mbano Mbano", "Android Developer"));
      userList.add(new User(3, "Mike Mbano", "C# Developer"));
      userList.add(new User(4, "File Mbano", "VB Developer"));
      
      return userList; 
   }
  
   public String getSAuth() {
	   String app_key = "BfbDerpLGClN5hCWbxydztQ5TzVpSpyA";
		String app_secret = "tTvnm7SOGN7WjHCJ";
				
				String appKeySecret = app_key + ":" + app_secret;
		byte[] bytes=null;
		try {
			bytes = appKeySecret.getBytes("ISO-8859-1");
		} catch (UnsupportedEncodingException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		String auth = Base64.getEncoder().encodeToString(bytes);

		OkHttpClient client = new OkHttpClient();

		Request request = new Request.Builder()
		  .url("https://api.safaricom.co.ke/oauth/v1/generate?grant_type=client_credentials")
		  .get()
		  .addHeader("authorization", "Basic " + auth)
		  .addHeader("cache-control", "no-cache")
		  .build();

		Response response=null;
		do {
			try{
			response = client.newCall(request).execute();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				//e.printStackTrace();
				response=null;
				continue;
			}
			
			}while(response==null);
		//System.out.println(response);
		//System.out.println("\n");
		String jsonData="";
		try {
			jsonData = response.body().string();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		JSONObject Jobject=null;
	    try {
		Jobject = new JSONObject(jsonData);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//System.out.println(response);
	String token="";
		//System.out.println(Jobject.toString());
		for(int i = 0; i<Jobject.names().length(); i++){
		   try {
			//System.out.println("key = " + Jobject.names().getString(i) + " value = " + Jobject.get(Jobject.names().getString(i)));
			   if(i==0) {
		    	   token=Jobject.get(Jobject.names().getString(i)).toString();
		       }
		   } catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		  
		}
		return token;
	
   }

   public String getCAuth() {
	   String app_key = "JnvQjNg3vo9VX7cyKPz1H8v2r8aDI1H7";
		String app_secret = "7uSw0W2pJFUVbkHp";
		String appKeySecret = app_key + ":" + app_secret;
		byte[] bytes=null;
		try {
			bytes = appKeySecret.getBytes("ISO-8859-1");
		} catch (UnsupportedEncodingException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		String auth = Base64.getEncoder().encodeToString(bytes);

		OkHttpClient client = new OkHttpClient();

		Request request = new Request.Builder()
		  .url("https://api.safaricom.co.ke/oauth/v1/generate?grant_type=client_credentials")
		  .get()
		  .addHeader("authorization", "Basic " + auth)
		  .addHeader("cache-control", "no-cache")
		  .build();

		Response response=null;
		do {
			try{
			response = client.newCall(request).execute();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				//e.printStackTrace();
				response=null;
				continue;
			}
			
			}while(response==null);
		//System.out.println(response);
		//System.out.println("\n");
		String jsonData="";
		try {
			jsonData = response.body().string();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		JSONObject Jobject=null;
	    try {
		Jobject = new JSONObject(jsonData);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//System.out.println(response);
	String token="";
		//System.out.println(Jobject.toString());
		for(int i = 0; i<Jobject.names().length(); i++){
		   try {
			//System.out.println("key = " + Jobject.names().getString(i) + " value = " + Jobject.get(Jobject.names().getString(i)));
			   if(i==0) {
		    	   token=Jobject.get(Jobject.names().getString(i)).toString();
		       }
		   } catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		  
		}
		return token;
	
   }

   
   public String getApp(String s,String c,String a,String m,String b) {
	   OkHttpClient client = new OkHttpClient();
String neme="";
do {
	neme=getSAuth();
	
}while(neme=="");

		MediaType mediaType = MediaType.parse("application/json");
		RequestBody body = RequestBody.create(mediaType,"{\"ShortCode\":\""+s+"\","
		  +"\"CommandID\":\""+c+"\","
		  +"\"Amount\":\""+a+"\","+
		  "\"Msisdn\":\""+m+"\","+
		  "\"BillRefNumber\":\""+b+"\" }");
		Request request = new Request.Builder()
		  .url("https://api.safaricom.co.ke/mpesa/c2b/v1/simulate")
		  .post(body)
		  .addHeader("authorization", "Bearer "+neme)
		  .addHeader("content-type", "application/json")
		  .build();
		Response response=null;
		do {
		try{
		response = client.newCall(request).execute();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
			response=null;
			continue;
		}
		
		}while(response==null);
		//System.out.println(response);
		//System.out.println("\n");
		String jsonData="";
		try {
			jsonData = response.body().string();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		JSONObject Jobject=null;
	    try {
		Jobject = new JSONObject(jsonData);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//System.out.println(response);
	    String tem="";
	   if(response.message().equals("OK")){
		   
		   tem="Activation request was processed successfully";
	   }		   
	   else{
		   tem=response.message();
	   }
	   
		return tem;
	
   }
   
   public String getAppC(String i,String c,String a,String pa,String pb,String rem) {
	   OkHttpClient client = new OkHttpClient();
	   String neme="";
	   do {
	   	neme=getCAuth();
	   	
	   }while(neme=="");
	//ZrL+GevAFSVUTaMlBEMTrlIQmLgHqyC1XCgBZFR8P5hX+0b3+AD0f5NfDv+xrkv8MEXq1z17LFu7y9kgargxpRyjJuyebPcbdZpUpptfXAubsqO3OaOwFktvIrBHStwhmuKlXDla5cirYgE6EHgNT/kwseJWvjAvT5NIUdzqbX1VQAxLEss6m9UBLmhpoQJBYJ49dK43nzmnNhHa2PE04VJdV0uf/3FAbuaN73KEl/LrB8BwA6JppQ9x+XHtzlMs0F7HtzLigR09VSx98T2es6rcGGLRTn5iOgIk1Y41zj3GDt5o6oU9HVMxCFjVEL+pB8ciz7zLeK2b8Fs29EECCg==
	   MediaType mediaType = MediaType.parse("application/json");
		RequestBody body = RequestBody.create(mediaType, "{\"InitiatorName\":\""+i+"\","
				  +"\"SecurityCredential\":\"YeWMuWZdHtmAgX+NguX9TNpX4IULva7G65RblP+VkECpZymb4NckrESMl2eH57fWVmFqxNbdR0N3d9xfv5S9g8Vqw6P0GzOEcUvo7wp2hFELC0XoMRcsfglHaiH2onWQS4BEX9N7SVdzGop/cQzYEX+YSQawR66koI0TQZepdfvuuALsiObz0i/GoLFerOse0KM0kXJsMYxJmKSg4Wf0oxClGrnDVGqzhkCRzvfcIi5o8H8zyhmDf+8EbL5gIz62G+WbQ0TgdOw3j76Vi0MtZlzhLKMeDMic3t0Zv7ZPHZcoEabp+eNXzCeeRFf08NEmRUIVDyTdGo9qkT5nKaMAtA==\","
				  +"\"CommandID\":\""+c+"\","
				  +"\"Amount\":\""+a+"\","
				  +"\"PartyA\":\""+pa+"\","
				  +"\"PartyB\":\""+pb+"\","
				  +"\"Remarks\":\""+rem+"\","
				  +"\"QueueTimeOutURL\":\"https://f1.cdsckenya.com/DirectXML/Responder.aspx\","
				  +"\"ResultURL\":\"https://f1.cdsckenya.com/DirectXML/Responder.aspx\"}");
		Request request = new Request.Builder()
		  .url("https://api.safaricom.co.ke/mpesa/b2c/v1/paymentrequest")
		  .post(body)
		  .addHeader("authorization", "Bearer "+neme)
		  .addHeader("content-type", "application/json")
		  .build();
		Response response=null;
		do {
		try{
		response = client.newCall(request).execute();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
			response=null;
			continue;
		}
		
		}while(response==null);
		//System.out.println(response);
		//System.out.println("\n");
		String jsonData="";
		try {
			jsonData = response.body().string();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		JSONObject Jobject=null;
	    try {
		Jobject = new JSONObject(jsonData);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//System.out.println(response);
	
		return Jobject.toString();
	   
   }
   

	
	 public String getAppF(String mobi,String cds,String amount,String timestamp) {
		   OkHttpClient client = new OkHttpClient();
	String neme="";
	do {
		neme=getSAuth();
		
	}while(neme=="");
	amount=amount.replaceAll("-",".");
	byte[] bytes2=null;

	LocalDateTime today = LocalDateTime.now();
	DateTimeFormatter format;
	format = DateTimeFormatter.ofPattern("yyyymmddHHmmss");
	//String TimeStamp=today.format(format).toString();
	String ain="272007f1a9bdea75f9dd2e980826316a2c6ff663eff30f88fcf4d0a9f49c75509e2390"+timestamp;
	try {
		bytes2 = ain.getBytes("ISO-8859-1");
	} catch (UnsupportedEncodingException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	String auth2= Base64.getEncoder().encodeToString(bytes2);
	MediaType mediaType = MediaType.parse("application/json");
	RequestBody body = RequestBody.create(mediaType,"{\"BusinessShortCode\":\"272007\","
	  +"\"Password\":\""+ auth2 +"\","
	  +"\"Timestamp\":\""+ timestamp +"\","+
	  "\"TransactionType\":\"CustomerPayBillOnline\","+
	  "\"Amount\":\""+ amount +"\","+
	  "\"PartyA\":\""+ mobi +"\","+
	  "\"PartyB\":\"272007\","+
	  "\"PhoneNumber\":\""+ mobi +"\","+
	  "\"CallBackURL\":\"https://f3.cdsckenya.com/CDSCW/Responder.aspx\","+
	  "\"AccountReference\":\""+ cds +"\","+
	  "\"TransactionDesc\":\"SUBSCRIPTION\" }");
	Request request = new Request.Builder()
	           .url("https://api.safaricom.co.ke/mpesa/stkpush/v1/processrequest")
	           .post(body)
			  .addHeader("authorization", "Bearer "+neme)
			  .addHeader("content-type", "application/json")
			  .build();
	Response response=null;
	try{
	response = client.newCall(request).execute();
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}

	//System.out.println(response);
	//System.out.println("\n");
	String jsonData="";
	try {
		jsonData = response.body().string();
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	JSONObject Jobject=null;
	try {
	Jobject = new JSONObject(jsonData);
	} catch (JSONException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}


	//System.out.println(Jobject.toString());

			return Jobject.toString();
		
	   }
	   public String getAppN(String req,String timestamp) {
		   OkHttpClient client = new OkHttpClient();
	String neme="";
	do {
		neme=getSAuth();
		
	}while(neme=="");
	LocalDateTime today = LocalDateTime.now();
	DateTimeFormatter format;
	format = DateTimeFormatter.ofPattern("yyyymmddHHmmss");
	//String TimeStamp=today.format(format).toString();
	String ain="272007f1a9bdea75f9dd2e980826316a2c6ff663eff30f88fcf4d0a9f49c75509e2390"+timestamp;
	byte[] bytes2=null;
	try {
		bytes2 = ain.getBytes("ISO-8859-1");
	} catch (UnsupportedEncodingException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	String auth2= Base64.getEncoder().encodeToString(bytes2);

	MediaType mediaType = MediaType.parse("application/json");
	RequestBody body = RequestBody.create(mediaType,"{\"BusinessShortCode\":\"272007\","
	  +"\"Password\":\""+ auth2 +"\","
	  +"\"Timestamp\":\""+ timestamp +"\","+
	  "\"CheckoutRequestID\":\""+ req +"\" }");
	Request request = new Request.Builder()
	  .url("https://api.safaricom.co.ke/mpesa/stkpushquery/v1/query")
	  .post(body)
	  .addHeader("authorization", "Bearer "+neme)
	  .addHeader("content-type", "application/json")
	  .build();
	Response response=null;
	try{
	response = client.newCall(request).execute();
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}

	//System.out.println(response);
	//System.out.println("\n");
	String jsonData="";
	try {
		jsonData = response.body().string();
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	JSONObject Jobject=null;
	try {
	Jobject = new JSONObject(jsonData);
	} catch (JSONException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}

			
	return Jobject.toString();
		
	   }


   }
