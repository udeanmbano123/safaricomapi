package com.service;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
@XmlRootElement(name = "DATASETCDA") 
public class DATASET_CDA implements Serializable {  
	   private static final long serialVersionUID = 1L; 
	public DATASET_CDA() {}
	 public String getMEMBER_CODE() {
		return MEMBER_CODE;
	}
	 @XmlElement 
	public void setMEMBER_CODE(String mEMBER_CODE) {
		MEMBER_CODE = mEMBER_CODE;
	}
	public String getMEMBER_TYPE() {
		return MEMBER_TYPE;
	}
	 @XmlElement 
	public void setMEMBER_TYPE(String mEMBER_TYPE) {
		MEMBER_TYPE = mEMBER_TYPE;
	}
	public String getNAME() {
		return NAME;
	}
	 @XmlElement 
	public void setNAME(String nAME) {
		NAME = nAME;
	}
	public String getADDRESS1() {
		return ADDRESS1;
	}
	 @XmlElement 
	public void setADDRESS1(String aDDRESS1) {
		ADDRESS1 = aDDRESS1;
	}
	public String getADDRESS2() {
		return ADDRESS2;
	}
	 @XmlElement 
	public void setADDRESS2(String aDDRESS2) {
		ADDRESS2 = aDDRESS2;
	}
	public String getADDRESS3() {
		return ADDRESS3;
	}
	 @XmlElement 
	public void setADDRESS3(String aDDRESS3) {
		ADDRESS3 = aDDRESS3;
	}
	public String getTOWN() {
		return TOWN;
	}
	 @XmlElement 
	public void setTOWN(String tOWN) {
		TOWN = tOWN;
	}
	public String getPOST_CODE() {
		return POST_CODE;
	}
	 @XmlElement 
	public void setPOST_CODE(String pOST_CODE) {
		POST_CODE = pOST_CODE;
	}
	public String getCOUNTRY_CODE() {
		return COUNTRY_CODE;
	}
	 @XmlElement 
	public void setCOUNTRY_CODE(String cOUNTRY_CODE) {
		COUNTRY_CODE = cOUNTRY_CODE;
	}
	public String getTELEPHONE() {
		return TELEPHONE;
	}
	 @XmlElement 
	public void setTELEPHONE(String tELEPHONE) {
		TELEPHONE = tELEPHONE;
	}
	public String getFax() {
		return fax;
	}
	 @XmlElement 
	public void setFax(String fax) {
		this.fax = fax;
	}
	public String getE_MAIL() {
		return E_MAIL;
	}
	 @XmlElement 
	public void setE_MAIL(String e_MAIL) {
		E_MAIL = e_MAIL;
	}
	public String getSTATUS() {
		return STATUS;
	}
	 @XmlElement 
	public void setSTATUS(String sTATUS) {
		STATUS = sTATUS;
	}
	public String getStatus_description() {
		return status_description;
	}
	 @XmlElement 
	public void setStatus_description(String status_description) {
		this.status_description = status_description;
	}

	 public DATASET_CDA(String mEMBER_CODE, String mEMBER_TYPE, String nAME, String aDDRESS1, String aDDRESS2,
			String aDDRESS3, String tOWN, String pOST_CODE, String cOUNTRY_CODE, String tELEPHONE, String fax,
			String e_MAIL, String sTATUS, String status_description) {
		super();
		MEMBER_CODE = mEMBER_CODE;
		MEMBER_TYPE = mEMBER_TYPE;
		NAME = nAME;
		ADDRESS1 = aDDRESS1;
		ADDRESS2 = aDDRESS2;
		ADDRESS3 = aDDRESS3;
		TOWN = tOWN;
		POST_CODE = pOST_CODE;
		COUNTRY_CODE = cOUNTRY_CODE;
		TELEPHONE = tELEPHONE;
		this.fax = fax;
		E_MAIL = e_MAIL;
		STATUS = sTATUS;
		this.status_description = status_description;
	}	
	 private String MEMBER_CODE;
	private String	MEMBER_TYPE;
	 private String NAME;
	 private String ADDRESS1;
	 private String ADDRESS2;
	 private String ADDRESS3;
	 private String TOWN;
	 private String POST_CODE;
	 private String COUNTRY_CODE;
	 private String TELEPHONE;
	 private String fax;
	 private String E_MAIL;
	 private String STATUS;
	 private String status_description;
}
