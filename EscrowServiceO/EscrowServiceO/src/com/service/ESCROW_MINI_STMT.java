package com.service;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
@XmlRootElement(name = "ESCROWMINISTMT") 
public class ESCROW_MINI_STMT implements Serializable {  
	   private static final long serialVersionUID = 1L;
	public ESCROW_MINI_STMT() {}
	public String getCLIENT_PREFIX() {
		return CLIENT_PREFIX;
	}
	 @XmlElement 
	public void setCLIENT_PREFIX(String cLIENT_PREFIX) {
		CLIENT_PREFIX = cLIENT_PREFIX;
	}
	public String getMEMBER_CODE() {
		return MEMBER_CODE;
	}
	 @XmlElement 
	public void setMEMBER_CODE(String mEMBER_CODE) {
		MEMBER_CODE = mEMBER_CODE;
	}
	public String getISSUER_CODE() {
		return ISSUER_CODE;
	}
	 @XmlElement 
	public void setISSUER_CODE(String iSSUER_CODE) {
		ISSUER_CODE = iSSUER_CODE;
	}
	public String getMAIN_TYPE() {
		return MAIN_TYPE;
	}
	 @XmlElement 
	public void setMAIN_TYPE(String mAIN_TYPE) {
		MAIN_TYPE = mAIN_TYPE;
	}
	public String getDescription() {
		return description;
	}
	 @XmlElement 
	public void setDescription(String description) {
		this.description = description;
	}
	public String getDate_system() {
		return date_system;
	}
	 @XmlElement 
	public void setDate_system(String date_system) {
		this.date_system = date_system;
	}
	public String getTshares() {
		return tshares;
	}
	 @XmlElement 
	public void setTshares(String tshares) {
		this.tshares = tshares;
	}
	
	public ESCROW_MINI_STMT(String cLIENT_PREFIX, String mEMBER_CODE, String iSSUER_CODE, String mAIN_TYPE,
			String description, String date_system, String tshares) {
		super();
		CLIENT_PREFIX = cLIENT_PREFIX;
		MEMBER_CODE = mEMBER_CODE;
		ISSUER_CODE = iSSUER_CODE;
		MAIN_TYPE = mAIN_TYPE;
		this.description = description;
		this.date_system = date_system;
		this.tshares = tshares;
	}
	private String CLIENT_PREFIX;
	private String MEMBER_CODE;
	private String ISSUER_CODE; 
	private String MAIN_TYPE;
	private String description; 
	 private String date_system; 
	 private String tshares;
}
