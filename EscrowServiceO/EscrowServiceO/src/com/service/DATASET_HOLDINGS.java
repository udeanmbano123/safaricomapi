package com.service;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
@XmlRootElement(name =
"DATASETHOLDINGS") 
public class DATASET_HOLDINGS implements Serializable {  
	   private static final long serialVersionUID = 1L; 
	   
		public DATASET_HOLDINGS() {}
	 public String getCLIENT_PREFIX() {
		return CLIENT_PREFIX;
	}
	 @XmlElement 
	public void setCLIENT_PREFIX(String cLIENT_PREFIX) {
		CLIENT_PREFIX = cLIENT_PREFIX;
	}
	public String getCLIENT_SUFFIX() {
		return CLIENT_SUFFIX;
	}
	 @XmlElement 
	public void setCLIENT_SUFFIX(String cLIENT_SUFFIX) {
		CLIENT_SUFFIX = cLIENT_SUFFIX;
	}
	public String getMEMBER_CODE() {
		return MEMBER_CODE;
	}
	 @XmlElement 
	public void setMEMBER_CODE(String mEMBER_CODE) {
		MEMBER_CODE = mEMBER_CODE;
	}
	public String getMEMBER_TYPE() {
		return MEMBER_TYPE;
	}
	 @XmlElement 
	public void setMEMBER_TYPE(String mEMBER_TYPE) {
		MEMBER_TYPE = mEMBER_TYPE;
	}
	public String getSHORT_NAME() {
		return SHORT_NAME;
	}
	 @XmlElement 
	public void setSHORT_NAME(String sHORT_NAME) {
		SHORT_NAME = sHORT_NAME;
	}
	public String getISSUER_CODE() {
		return ISSUER_CODE;
	}
	 @XmlElement 
	public void setISSUER_CODE(String iSSUER_CODE) {
		ISSUER_CODE = iSSUER_CODE;
	}
	public String getMAIN_TYPE() {
		return MAIN_TYPE;
	}
	 @XmlElement 
	public void setMAIN_TYPE(String mAIN_TYPE) {
		MAIN_TYPE = mAIN_TYPE;
	}
	public String getSUB_TYPE() {
		return SUB_TYPE;
	}
	 @XmlElement 
	public void setSUB_TYPE(String sUB_TYPE) {
		SUB_TYPE = sUB_TYPE;
	}
	public String getQnty_held() {
		return qnty_held;
	}
	 @XmlElement 
	public void setQnty_held(String qnty_held) {
		this.qnty_held = qnty_held;
	}
	public String getDATE_LAST_TRANSACTION() {
		return DATE_LAST_TRANSACTION;
	}
	 @XmlElement 
	public void setDATE_LAST_TRANSACTION(String dATE_LAST_TRANSACTION) {
		DATE_LAST_TRANSACTION = dATE_LAST_TRANSACTION;
	}
	public String getSec_ACC_TYPE() {
		return sec_ACC_TYPE;
	}
	 @XmlElement 
	public void setSec_ACC_TYPE(String sec_ACC_TYPE) {
		this.sec_ACC_TYPE = sec_ACC_TYPE;
	}
	
	 public DATASET_HOLDINGS(String cLIENT_PREFIX, String cLIENT_SUFFIX, String mEMBER_CODE, String mEMBER_TYPE,
			String sHORT_NAME, String iSSUER_CODE, String mAIN_TYPE, String sUB_TYPE, String qnty_held,
			String dATE_LAST_TRANSACTION, String sec_ACC_TYPE) {
		super();
		CLIENT_PREFIX = cLIENT_PREFIX;
		CLIENT_SUFFIX = cLIENT_SUFFIX;
		MEMBER_CODE = mEMBER_CODE;
		MEMBER_TYPE = mEMBER_TYPE;
		SHORT_NAME = sHORT_NAME;
		ISSUER_CODE = iSSUER_CODE;
		MAIN_TYPE = mAIN_TYPE;
		SUB_TYPE = sUB_TYPE;
		this.qnty_held = qnty_held;
		DATE_LAST_TRANSACTION = dATE_LAST_TRANSACTION;
		this.sec_ACC_TYPE = sec_ACC_TYPE;
	}
	 private String CLIENT_PREFIX;
	private String CLIENT_SUFFIX;
	 private String MEMBER_CODE;
	 private String MEMBER_TYPE;
	 private String SHORT_NAME;
	 private String ISSUER_CODE;
	 private String MAIN_TYPE;
	 private String SUB_TYPE;
	 private String qnty_held;
	 private String DATE_LAST_TRANSACTION;
	 private String sec_ACC_TYPE;
	 
	 
}
