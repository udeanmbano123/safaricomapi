package com.service;

import java.util.List; 
import javax.ws.rs.GET; 
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces; 
import javax.ws.rs.core.MediaType;  
@Path("/OracleService") 

public class OracleService {  
   UserDAO userDao = new UserDAO();   
   @GET 
   @Path("/test/{id}/{id2}") 
   @Produces(MediaType.TEXT_HTML) 
   public String test(@PathParam("id") String id,@PathParam("id2") String id2){ 
      return "You requested "+ id + " marry " + id2; 
   }  
   
   @GET 
   @Path("/users") 
   @Produces(MediaType.APPLICATION_XML) 
   public List<User> getUsers(){ 
      return userDao.getAllUsers(); 
   }  
   
   @GET 
   @Path("/dtcda") 
   @Produces(MediaType.APPLICATION_XML) 
   public List<DATASET_CDA> getDATASETCDA(){ 
      return userDao.getAllDATASET_CDA(); 
   }  
   
   @GET 
   @Path("/dthold/{s}") 
   @Produces(MediaType.APPLICATION_XML) 
   public List<DATASET_HOLDINGS> getDATASETHOLD(@PathParam("s") String s){ 
      return userDao.getAllDATASET_HOLD(s); 
   } 
  
 
   @GET 
   @Path("/dtissue") 
   @Produces(MediaType.APPLICATION_XML) 
   public List<DATASET_ISSUERS> getDATASETISSUERS(){ 
      return userDao.getAllDATASET_ISSUERS(); 
   } 
   @GET 
   @Path("/dtprices") 
   @Produces(MediaType.APPLICATION_XML) 
   public List<DATASET_PRICES> getDATASETPRICES(){ 
      return userDao.getAllDATASET_PRICES(); 
   } 
   
   @GET 
   @Path("/dtpricesL") 
   @Produces(MediaType.APPLICATION_XML) 
   public List<DATASET_PRICES> getDATASETPRICESL(){ 
      return userDao.getAllDATASET_PRICESL(); 
   }
   @GET 
   @Path("/dtpricesG") 
   @Produces(MediaType.APPLICATION_XML) 
   public List<DATASET_PRICES> getDATASETPRICESG(){ 
      return userDao.getAllDATASET_PRICESG(); 
   }
   @GET 
   @Path("/dtclients/{account}") 
   @Produces(MediaType.APPLICATION_XML) 
   public List<ESCROW_CLIENTS> getESCROWCLIENTS(@PathParam("account") String account){ 
      return userDao.getAllESCROW_CLIENTS(account); 
   }
   @GET 
   @Path("/dtclientsA/{account}") 
   @Produces(MediaType.APPLICATION_XML) 
   public List<ESCROW_CLIENTS> getESCROWCLIENTS2(@PathParam("account") String account){ 
      return userDao.getAllESCROW_CLIENTS2(account); 
   }
   @GET 
   @Path("/dtnumber/{account}") 
   @Produces(MediaType.WILDCARD) 
   public String getnumber(@PathParam("account") String account){ 
      return userDao.getNumber(account); 
   }
  
   @GET 
   @Path("/dtnames/{account}") 
   @Produces(MediaType.WILDCARD) 
   public String getNames(@PathParam("account") String account){ 
      return userDao.getNames(account); 
   }
   @GET 
   @Path("/did/{account}") 
   @Produces(MediaType.WILDCARD) 
   public String getID(@PathParam("account") String account){ 
      return userDao.getID(account); 
   }
   @GET 
   @Path("/dtstmt/{account}/{dt1}/{dt2}") 
   @Produces(MediaType.APPLICATION_XML) 
   public List<ESCROW_MINI_STMT> getDATASETESCROW_MINI_STMT(@PathParam("account") String account,@PathParam("dt1") String dt1,@PathParam("dt2") String dt2){ 
      return userDao.getAllESCROW_MINI_STMT(account,dt1,dt2); 
   } 
   @GET 
   @Path("/dtport/{account}") 
   @Produces(MediaType.APPLICATION_XML) 
   public List<Portfolio> getPort(@PathParam("account") String account){ 
      return userDao.getAllPortfolio(account); 
   }   
   @GET 
   @Path("/dtcprice/{account}") 
   @Produces(MediaType.WILDCARD) 
   public String getPort2(@PathParam("account") String account){ 
      return userDao.getIssuerPrice(account); 
   }
   @GET 
   @Path("/demail/{account}") 
   @Produces(MediaType.WILDCARD) 
   public String getPort222(@PathParam("account") String account){ 
      return userDao.getEmail(account); 
   }
   @GET 
   @Path("issuelist") 
   @Produces(MediaType.APPLICATION_XML) 
   public List<IssuerList> getPo(){ 
      return userDao.getIssuerss(); 
   }
   @GET 
   @Path("/dalert/{account}") 
   @Produces(MediaType.APPLICATION_XML) 
   public List<ALERTS> getAlert(@PathParam("account") String account){ 
      return userDao.getAllALERTS(account); 
   }
   @GET 
   @Path("/dtrade/{account}") 
   @Produces(MediaType.APPLICATION_XML) 
   public List<TRADES> getTrades(@PathParam("account") String account){ 
      return userDao.getAllTRADES(account); 
   }
   
   @GET 
   @Path("/hold/{s}") 
   @Produces(MediaType.APPLICATION_XML) 
   public List<HOLDINGS> HDS(@PathParam("s") String s){ 
      return userDao.HDS(s);  
   } 
   
   @GET 
   @Path("/members") 
   @Produces(MediaType.APPLICATION_XML) 
   public List<MEMBERS> HDSS(@PathParam("s") String s){ 
      return userDao.MEM("s");  
   } 
   
   @GET 
   @Path("/banks") 
   @Produces(MediaType.APPLICATION_XML) 
   public List<BANK> HDSSS(@PathParam("s") String s){ 
      return userDao.BAN("s");  
   } 
   
}
