package com.service;

import java.io.File; 
import java.io.FileInputStream; 
import java.io.FileNotFoundException;  
import java.io.FileOutputStream; 
import java.io.IOException; 
import java.io.ObjectInputStream; 
import java.io.ObjectOutputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;  
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;
import java.util.TimeZone;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class UserDAO {  
	
        
   public List<User> getAllUsers(){ 
      
      List<User> userList =new ArrayList<User>(); 
    userList.add(new User(1, "Udean Mbano", "Java Developer"));
      userList.add(new User(2, "Mbano Mbano", "Android Developer"));
      userList.add(new User(3, "Mike Mbano", "C# Developer"));
      userList.add(new User(4, "File Mbano", "VB Developer"));
      
      return userList; 
   }
  
   public String getSAuth() {
	   String app_key = "zTRLkvyRs7PWBu1w0OPneaKZ27fCseM9";
		String app_secret = "dl4kw5iaZWaAUF0Q";
				
				String appKeySecret = app_key + ":" + app_secret;
		byte[] bytes=null;
		try {
			bytes = appKeySecret.getBytes("ISO-8859-1");
		} catch (UnsupportedEncodingException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		String auth = Base64.getEncoder().encodeToString(bytes);

		OkHttpClient client = new OkHttpClient();

		Request request = new Request.Builder()
		  .url("https://api.safaricom.co.ke/oauth/v1/generate?grant_type=client_credentials")
		  .get()
		  .addHeader("authorization", "Basic " + auth)
		  .addHeader("cache-control", "no-cache")
		  .build();

		Response response=null;
		do {
			try{
			response = client.newCall(request).execute();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				//e.printStackTrace();
				response=null;
				continue;
			}
			
			}while(response==null);
		//System.out.println(response);
		//System.out.println("\n");
		String jsonData="";
		try {
			jsonData = response.body().string();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		JSONObject Jobject=null;
	    try {
		Jobject = new JSONObject(jsonData);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//System.out.println(response);
	String token="";
		//System.out.println(Jobject.toString());
		for(int i = 0; i<Jobject.names().length(); i++){
		   try {
			//System.out.println("key = " + Jobject.names().getString(i) + " value = " + Jobject.get(Jobject.names().getString(i)));
			   if(i==0) {
		    	   token=Jobject.get(Jobject.names().getString(i)).toString();
		       }
		   } catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		  
		}
		return token;
	
   }

   public String getCAuth() {
	   String app_key = "JnvQjNg3vo9VX7cyKPz1H8v2r8aDI1H7";
		String app_secret = "7uSw0W2pJFUVbkHp";
		String appKeySecret = app_key + ":" + app_secret;
		byte[] bytes=null;
		try {
			bytes = appKeySecret.getBytes("ISO-8859-1");
		} catch (UnsupportedEncodingException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		String auth = Base64.getEncoder().encodeToString(bytes);

		OkHttpClient client = new OkHttpClient();

		Request request = new Request.Builder()
		  .url("https://api.safaricom.co.ke/oauth/v1/generate?grant_type=client_credentials")
		  .get()
		  .addHeader("authorization", "Basic " + auth)
		  .addHeader("cache-control", "no-cache")
		  .build();

		Response response=null;
		do {
			try{
			response = client.newCall(request).execute();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				//e.printStackTrace();
				response=null;
				continue;
			}
			
			}while(response==null);
		//System.out.println(response);
		//System.out.println("\n");
		String jsonData="";
		try {
			jsonData = response.body().string();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		JSONObject Jobject=null;
	    try {
		Jobject = new JSONObject(jsonData);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//System.out.println(response);
	String token="";
		//System.out.println(Jobject.toString());
		for(int i = 0; i<Jobject.names().length(); i++){
		   try {
			//System.out.println("key = " + Jobject.names().getString(i) + " value = " + Jobject.get(Jobject.names().getString(i)));
			   if(i==0) {
		    	   token=Jobject.get(Jobject.names().getString(i)).toString();
		       }
		   } catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		  
		}
		return token;
	
   }

   
   public String getApp(String s,String c,String a,String m,String b) {
	   OkHttpClient client = new OkHttpClient();
String neme="";
do {
	neme=getSAuth();
	
}while(neme=="");

		MediaType mediaType = MediaType.parse("application/json");
		RequestBody body = RequestBody.create(mediaType,"{\"ShortCode\":\""+s+"\","
		  +"\"CommandID\":\""+c+"\","
		  +"\"Amount\":\""+a+"\","+
		  "\"Msisdn\":\""+m+"\","+
		  "\"BillRefNumber\":\""+b+"\" }");
		Request request = new Request.Builder()
		  .url("https://api.safaricom.co.ke/mpesa/c2b/v1/simulate")
		  .post(body)
		  .addHeader("authorization", "Bearer "+neme)
		  .addHeader("content-type", "application/json")
		  .build();
		Response response=null;
		do {
		try{
		response = client.newCall(request).execute();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
			response=null;
			continue;
		}
		
		}while(response==null);
		//System.out.println(response);
		//System.out.println("\n");
		String jsonData="";
		try {
			jsonData = response.body().string();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		JSONObject Jobject=null;
	    try {
		Jobject = new JSONObject(jsonData);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//System.out.println(response);
	    String tem="";
	   if(response.message().equals("OK")){
		   
		   tem="Activation request was processed successfully";
	   }		   
	   else{
		   tem=response.message();
	   }
	   
		return tem;
	
   }
   
   public String getAppC(String i,String c,String a,String pa,String pb,String rem) {
	   OkHttpClient client = new OkHttpClient();
	   String neme="";
	   do {
	   	neme=getCAuth();
	   	
	   }while(neme=="");
	//Xvu+rh+ibPkxSXkqXQIR3iPIkSqyVEriDGGZf0Ag/u1xwrwXNxPwpPTwt6vidyVEdnhOPvqRXtDFlo4hmeEkcFhHx7uG68RAELAQt/w+SZbdQCGI0hObGuMeTF3niC6s4B6+M9ovxikngrG4oFpnwydIHtvVWR602qPS/Sdnc7bgZ6qXwF1Mhihn+k/sBUdmMgyaCYgdaoqn+LeengkMxJMIOxebEX/9K7nwdozk4pV7ip+freU7j+3H9nSx7232yBUGN4u0lgmc/baelBzlATxnphAu0RrOQZFrmRL+k6q0hHCPY5nwqb6ETgBbn3FB14AkCk8qBDPsRXfCuku+NA==
	   MediaType mediaType = MediaType.parse("application/json");
		RequestBody body = RequestBody.create(mediaType, "{\"InitiatorName\":\""+i+"\","
				  +"\"SecurityCredential\":\"ZrL+GevAFSVUTaMlBEMTrlIQmLgHqyC1XCgBZFR8P5hX+0b3+AD0f5NfDv+xrkv8MEXq1z17LFu7y9kgargxpRyjJuyebPcbdZpUpptfXAubsqO3OaOwFktvIrBHStwhmuKlXDla5cirYgE6EHgNT/kwseJWvjAvT5NIUdzqbX1VQAxLEss6m9UBLmhpoQJBYJ49dK43nzmnNhHa2PE04VJdV0uf/3FAbuaN73KEl/LrB8BwA6JppQ9x+XHtzlMs0F7HtzLigR09VSx98T2es6rcGGLRTn5iOgIk1Y41zj3GDt5o6oU9HVMxCFjVEL+pB8ciz7zLeK2b8Fs29EECCg==\","
				  +"\"CommandID\":\""+c+"\","
				  +"\"Amount\":\""+a+"\","
				  +"\"PartyA\":\""+pa+"\","
				  +"\"PartyB\":\""+pb+"\","
				  +"\"Remarks\":\""+rem+"\","
				  +"\"QueueTimeOutURL\":\"https://f1.cdsckenya.com/DirectXML/Responder.aspx\","
				  +"\"ResultURL\":\"https://f1.cdsckenya.com/DirectXML/Responder.aspx\"}");
		Request request = new Request.Builder()
		  .url("https://api.safaricom.co.ke/mpesa/b2c/v1/paymentrequest")
		  .post(body)
		  .addHeader("authorization", "Bearer "+neme)
		  .addHeader("content-type", "application/json")
		  .build();
		Response response=null;
		do {
		try{
		response = client.newCall(request).execute();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
			response=null;
			continue;
		}
		
		}while(response==null);
		//System.out.println(response);
		//System.out.println("\n");
		String jsonData="";
		try {
			jsonData = response.body().string();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		JSONObject Jobject=null;
	    try {
		Jobject = new JSONObject(jsonData);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//System.out.println(response);
	
		return Jobject.toString();
	   
   }

   }
