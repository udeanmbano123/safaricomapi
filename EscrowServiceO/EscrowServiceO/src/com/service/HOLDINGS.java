package com.service;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
@XmlRootElement(name = "HOLDINGS") 
public class HOLDINGS implements Serializable {  
	   private static final long serialVersionUID = 1L; 
	
	   public HOLDINGS() {}

	
	public HOLDINGS(String mEMBER_CODE, String iSSUER_CODE, String sHORT_NAME,String mAIN_TYPE,String sUB_TYPE, String qTY,String ISS) {
		super();
		MEMBER_CODE = mEMBER_CODE;
		ISSUER_CODE = iSSUER_CODE;
		SHORT_NAME = sHORT_NAME;
		QTY = qTY;
          MAIN_TYPE=mAIN_TYPE;
		   SUB_TYPE =sUB_TYPE;
		   ISSUER_NAME=ISS;
	}
	private String MAIN_TYPE;
	public String getMAIN_TYPE() {
		return MAIN_TYPE;
	}


	public void setMAIN_TYPE(String mAIN_TYPE) {
		MAIN_TYPE = mAIN_TYPE;
	}


	public String getSUB_TYPE() {
		return SUB_TYPE;
	}


	public void setSUB_TYPE(String sUB_TYPE) {
		SUB_TYPE = sUB_TYPE;
	}
	private String SUB_TYPE;
    private String MEMBER_CODE;
	public String getMEMBER_CODE() {
		return MEMBER_CODE;
	}


	public void setMEMBER_CODE(String mEMBER_CODE) {
		MEMBER_CODE = mEMBER_CODE;
	}


	public String getISSUER_CODE() {
		return ISSUER_CODE;
	}


	public void setISSUER_CODE(String iSSUER_CODE) {
		ISSUER_CODE = iSSUER_CODE;
	}


	public String getSHORT_NAME() {
		return SHORT_NAME;
	}


	public void setSHORT_NAME(String sHORT_NAME) {
		SHORT_NAME = sHORT_NAME;
	}


	public String getQTY() {
		return QTY;
	}


	public void setQTY(String qTY) {
		QTY = qTY;
	}
	private String ISSUER_CODE;
	private String SHORT_NAME;
	private String QTY;
	private String ISSUER_NAME;

	public String getISSUER_NAME() {
		return ISSUER_NAME;
	}


	public void setISSUER_NAME(String iSSUER_NAME) {
		ISSUER_NAME = iSSUER_NAME;
	}

}
