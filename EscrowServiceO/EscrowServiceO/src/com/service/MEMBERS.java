package com.service;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
@XmlRootElement(name = "MEMBERS") 
public class MEMBERS implements Serializable {  
	   private static final long serialVersionUID = 1L; 
	
	   public MEMBERS() {}

	  
	   public String getMEMBER_CODE() {
		return MEMBER_CODE;
	}
	public void setMEMBER_CODE(String mEMBER_CODE) {
		MEMBER_CODE = mEMBER_CODE;
	}
	public String getMEMBER_TYPE() {
		return MEMBER_TYPE;
	}
	public void setMEMBER_TYPE(String mEMBER_TYPE) {
		MEMBER_TYPE = mEMBER_TYPE;
	}
	public String getNANE() {
		return NANE;
	}
	public void setNANE(String nANE) {
		NANE = nANE;
	}
	 private String MEMBER_CODE;
	public MEMBERS(String mEMBER_CODE, String mEMBER_TYPE, String nANE) {
		super();
		MEMBER_CODE = mEMBER_CODE;
		MEMBER_TYPE = mEMBER_TYPE;
		NANE = nANE;
	}
	private String MEMBER_TYPE;
	   private String NANE;
	
}
