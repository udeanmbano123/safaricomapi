package com.service;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
@XmlRootElement(name = "TRADES") 
public class TRADES implements Serializable {  
	   private static final long serialVersionUID = 1L;
   public TRADES(){};
   private String CDS_REFERENCE;
	public String getCDS_REFERENCE() {
	return CDS_REFERENCE;
}
public TRADES(String iSSUER_CODE, String mAIN_TYPE, String sUB_TYPE, String qTY, String pRICE,
			String dATE_TRADE, String nET_BUY_SELL, String mEMBER_CODE, String cLIENT_PREFIX, String cLIENT_SUFFIX,String cDS_REFERENCE) {
		super();
		CDS_REFERENCE = cDS_REFERENCE;
		ISSUER_CODE = iSSUER_CODE;
		MAIN_TYPE = mAIN_TYPE;
		SUB_TYPE = sUB_TYPE;
		QTY = qTY;
		PRICE = pRICE;
		DATE_TRADE = dATE_TRADE;
		NET_BUY_SELL = nET_BUY_SELL;
		MEMBER_CODE = mEMBER_CODE;
		CLIENT_PREFIX = cLIENT_PREFIX;
		CLIENT_SUFFIX = cLIENT_SUFFIX;
	}
public void setCDS_REFERENCE(String cDS_REFERENCE) {
	CDS_REFERENCE = cDS_REFERENCE;
}
	public String getISSUER_CODE() {
		return ISSUER_CODE;
	}
	public void setISSUER_CODE(String iSSUER_CODE) {
		ISSUER_CODE = iSSUER_CODE;
	}
	public String getMAIN_TYPE() {
		return MAIN_TYPE;
	}
	public void setMAIN_TYPE(String mAIN_TYPE) {
		MAIN_TYPE = mAIN_TYPE;
	}
	public String getSUB_TYPE() {
		return SUB_TYPE;
	}
	public void setSUB_TYPE(String sUB_TYPE) {
		SUB_TYPE = sUB_TYPE;
	}
	public String getQTY() {
		return QTY;
	}
	public void setQTY(String qTY) {
		QTY = qTY;
	}
	public String getPRICE() {
		return PRICE;
	}
	public void setPRICE(String pRICE) {
		PRICE = pRICE;
	}
	public String getDATE_TRADE() {
		return DATE_TRADE;
	}
	public void setDATE_TRADE(String dATE_TRADE) {
		DATE_TRADE = dATE_TRADE;
	}
	public String getNET_BUY_SELL() {
		return NET_BUY_SELL;
	}
	public void setNET_BUY_SELL(String nET_BUY_SELL) {
		NET_BUY_SELL = nET_BUY_SELL;
	}
	public String getMEMBER_CODE() {
		return MEMBER_CODE;
	}
	public void setMEMBER_CODE(String mEMBER_CODE) {
		MEMBER_CODE = mEMBER_CODE;
	}
	public String getCLIENT_PREFIX() {
		return CLIENT_PREFIX;
	}
	public void setCLIENT_PREFIX(String cLIENT_PREFIX) {
		CLIENT_PREFIX = cLIENT_PREFIX;
	}
	public String getCLIENT_SUFFIX() {
		return CLIENT_SUFFIX;
	}
	public void setCLIENT_SUFFIX(String cLIENT_SUFFIX) {
		CLIENT_SUFFIX = cLIENT_SUFFIX;
	}
	
	public TRADES(String iSSUER_CODE, String mAIN_TYPE, String sUB_TYPE, String qTY, String pRICE, String dATE_TRADE,
			String nET_BUY_SELL, String mEMBER_CODE, String cLIENT_PREFIX, String cLIENT_SUFFIX) {
		super();
		ISSUER_CODE = iSSUER_CODE;
		MAIN_TYPE = mAIN_TYPE;
		SUB_TYPE = sUB_TYPE;
		QTY = qTY;
		PRICE = pRICE;
		DATE_TRADE = dATE_TRADE;
		NET_BUY_SELL = nET_BUY_SELL;
		MEMBER_CODE = mEMBER_CODE;
		CLIENT_PREFIX = cLIENT_PREFIX;
		CLIENT_SUFFIX = cLIENT_SUFFIX;
	}
	private String ISSUER_CODE;
	private String MAIN_TYPE;
	private String SUB_TYPE;
	private String QTY;
	private String PRICE;
	private String DATE_TRADE;
	private String NET_BUY_SELL; 
	private String MEMBER_CODE;
	private String CLIENT_PREFIX;
	private String CLIENT_SUFFIX;

}
