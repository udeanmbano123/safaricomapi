package com.service;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
@XmlRootElement(name = "BANKS") 
public class BANK implements Serializable {  
	   private static final long serialVersionUID = 1L; 
	
	   public BANK() {}

	  
	   	public BANK(String bANK_CODE, String bANK_NAME, String bRANCH_CODE, String bRANCH_NAME) {
		super();
		BANK_CODE = bANK_CODE;
		BANK_NAME = bANK_NAME;
		BRANCH_CODE = bRANCH_CODE;
		BRANCH_NAME = bRANCH_NAME;
	}


		private String BANK_CODE;
	   public String getBANK_CODE() {
			return BANK_CODE;
		}


		public void setBANK_CODE(String bANK_CODE) {
			BANK_CODE = bANK_CODE;
		}


		public String getBANK_NAME() {
			return BANK_NAME;
		}


		public void setBANK_NAME(String bANK_NAME) {
			BANK_NAME = bANK_NAME;
		}


		public String getBRANCH_CODE() {
			return BRANCH_CODE;
		}


		public void setBRANCH_CODE(String bRANCH_CODE) {
			BRANCH_CODE = bRANCH_CODE;
		}


		public String getBRANCH_NAME() {
			return BRANCH_NAME;
		}

		public void setBRANCH_NAME(String bRANCH_NAME) {
			BRANCH_NAME = bRANCH_NAME;
		}


	private String BANK_NAME;
		private String BRANCH_CODE;
		   private String BRANCH_NAME;
	   
	
}
