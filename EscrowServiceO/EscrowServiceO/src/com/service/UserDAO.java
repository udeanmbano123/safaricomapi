package com.service;

import java.io.File; 
import java.io.FileInputStream; 
import java.io.FileNotFoundException;  
import java.io.FileOutputStream; 
import java.io.IOException; 
import java.io.ObjectInputStream; 
import java.io.ObjectOutputStream; 
import java.util.ArrayList; 
import java.util.List;  
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;
import java.util.TimeZone;

public class UserDAO {  
	
	
	  public List<User> getAllUsers(){ 
      
      List<User> userList =new ArrayList<User>(); 
    userList.add(new User(1, "Udean Mbano", "Java Developer"));
      userList.add(new User(2, "Mbano Mbano", "Android Developer"));
      userList.add(new User(3, "Mike Mbano", "C# Developer"));
      userList.add(new User(4, "File Mbano", "VB Developer"));
      
      return userList; 
   } 
   public List<DATASET_CDA> getAllDATASET_CDA(){ 
	   String connectionUrl = "jdbc:sqlserver://127.0.0.1:1433;" +
				"databaseName=OracleDB;integratedSecurity=true;";
	  java.sql.Connection conn = null;      
	      List<DATASET_CDA> userList =new ArrayList<DATASET_CDA>(); 
	    
	      try {
	    	Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
	    	conn = DriverManager.getConnection(connectionUrl);
	    	System.out.println("Connected to Sql \n");
			// String sql ="select sysdate as current_day from dual";
			String sql = "SELECT\n" + "	MEMBER_CODE,\n" + "	MEMBER_TYPE,\n" + "	NAME,\n" + "	ADDRESS1,\n"
					+ "	ADDRESS2,\n" + "	ADDRESS3,\n" + "	TOWN,\n" + "	POST_CODE,\n" + "	COUNTRY_CODE,\n"
					+ "	TELEPHONE,\n" + "	fax,\n" + "	E_MAIL,\n" + "	STATUS,\n" + "	status_description\n" + "FROM\n"
					+ "	DATASET_CDA";
			//creating PreparedStatement object to execute query
			PreparedStatement preStatement = conn.prepareStatement(sql);
			ResultSet result = preStatement.executeQuery();
			while (result.next()) {
				userList.add(new DATASET_CDA(result.getString("MEMBER_CODE"),result.getString("MEMBER_TYPE"),result.getString("NAME"),result.getString("ADDRESS1"),result.getString("ADDRESS2"),
						result.getString("ADDRESS3"),result.getString("TOWN"), result.getString("POST_CODE"),result.getString("COUNTRY_CODE"), result.getString("TELEPHONE"),result.getString("fax"),
						result.getString("E_MAIL"), result.getString("STATUS"), result.getString("status_description")));
			

			
			} 
		} catch (SQLException | ClassNotFoundException e) {
			// TODO: handle exception
			userList.add(new DATASET_CDA(e.toString(),e.toString(),e.toString(),e.toString(),e.toString(),
					e.toString(),e.toString(), e.toString(),e.toString(),e.toString(),e.toString(),
					e.toString(), e.toString(),e.toString()));
		}
		return userList; 
	   } 
    
   public List<DATASET_HOLDINGS> getAllDATASET_HOLD(String s){ 
	   String connectionUrl = "jdbc:sqlserver://127.0.0.1:1433;" +
				"databaseName=OracleDB;integratedSecurity=true;";
	  java.sql.Connection conn = null;      
	        
	      List<DATASET_HOLDINGS> userList =new ArrayList<DATASET_HOLDINGS>(); 
	      try {
			
	    	  Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
		    	conn = DriverManager.getConnection(connectionUrl);
		    	System.out.println("Connected to Sql \n");
			// String sql ="select sysdate as current_day from dual";
			String sql = "select CLIENT_PREFIX,CLIENT_SUFFIX,MEMBER_CODE,MEMBER_TYPE,SHORT_NAME,ISSUER_CODE,MAIN_TYPE,SUB_TYPE,qnty_held,CAST(DATE_LAST_TRANSACTION AS DAte) as DATE_LAST_TRANSACTION, sec_ACC_TYPE from DATASET_HOLDINGS where CLIENT_PREFIX='"+ s +"'  order by CAST(DATE_LAST_TRANSACTION as date) desc";
			//creating PreparedStatement object to execute query
			PreparedStatement preStatement = conn.prepareStatement(sql);
			ResultSet result = preStatement.executeQuery();
			while (result.next()) {
				userList.add(new DATASET_HOLDINGS(result.getString("CLIENT_PREFIX"),result.getString("CLIENT_SUFFIX"),result.getString("MEMBER_CODE"),result.getString("MEMBER_TYPE"),result.getString("SHORT_NAME"),
						result.getString("ISSUER_CODE"),result.getString("MAIN_TYPE"), result.getString("SUB_TYPE"),result.getString("qnty_held"), result.getString("DATE_LAST_TRANSACTION"),result.getString("sec_ACC_TYPE")));
			

			
			} 
		} catch (SQLException | ClassNotFoundException e) {
			// TODO: handle exception
			userList.add(new DATASET_HOLDINGS(e.toString(),e.toString(),e.toString(),e.toString(),e.toString(),
					e.toString(),e.toString(), e.toString(),e.toString(),e.toString(),e.toString()));
		}
	      return userList; 
	   } 
   
   public List<DATASET_ISSUERS> getAllDATASET_ISSUERS(){ 
	   String connectionUrl = "jdbc:sqlserver://127.0.0.1:1433;" +
				"databaseName=OracleDB;integratedSecurity=true;";
	  java.sql.Connection conn = null;      
	    
	      List<DATASET_ISSUERS> userList =new ArrayList<DATASET_ISSUERS>(); 
	      try {

	    	  //creating connection to Oracle database using JDBC
	    	  Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
		    	conn = DriverManager.getConnection(connectionUrl);
		    	System.out.println("Connected to Sql \n");
		    	// String sql ="select sysdate as current_day from dual";
			String sql = "select\r\n" + 
					" NAME,ISSUER_CODE,MAIN_TYPE,SUB_TYPE,ISIN,SHORT_NAME, FH_LIMIT,FOREIGN_HOLDING,PRICE_LAST_TRADED,COUNTRY_OF_INITIAL_ISSUE,\r\n" + 
					" PAR_VALUE,DATE_LAST_TRADED,QTY_ISSUED,qty_demmarted,PREVIOUS_CLOSE,INDEXED_PRICE,PREVIOUS_INDEXED_PRICE,DATE_LAST_INDEXED,INDEXED_QUANTITY \r\n" + 
					" ,AVERAGE_PRICE,SECURITY_CATEGORY,REFERENCE_PRICE, STATUS,status_description\r\n" + 
					"from DATASET_ISSUERS";
			//creating PreparedStatement object to execute query
			PreparedStatement preStatement = conn.prepareStatement(sql);
			ResultSet result = preStatement.executeQuery();
			while (result.next()) {
				userList.add(new DATASET_ISSUERS(result.getString("NAME"),result.getString("ISSUER_CODE"),result.getString("MAIN_TYPE"),result.getString("SUB_TYPE"),result.getString("ISIN"),result.getString("SHORT_NAME"),result.getString("FH_LIMIT"),result.getString("FOREIGN_HOLDING"),result.getString("PRICE_LAST_TRADED"),result.getString("COUNTRY_OF_INITIAL_ISSUE"),
						result.getString("PAR_VALUE"),result.getString("DATE_LAST_TRADED"),result.getString("QTY_ISSUED"),result.getString("qty_demmarted"),result.getString("PREVIOUS_CLOSE"),result.getString("INDEXED_PRICE"),result.getString("PREVIOUS_INDEXED_PRICE"),result.getString("DATE_LAST_INDEXED"),result.getString("INDEXED_QUANTITY"),result.getString("AVERAGE_PRICE"),result.getString("SECURITY_CATEGORY"),result.getString("REFERENCE_PRICE"), result.getString("STATUS"),result.getString("status_description")));
			

			
			} 
		} catch (SQLException | ClassNotFoundException e) {
			// TODO: handle exception
			userList.add(new DATASET_ISSUERS(e.toString(),e.toString(),e.toString(),e.toString(),e.toString(),
					e.toString(),e.toString(), e.toString(),e.toString(),e.toString(),e.toString(), null, null, null, null, null, null, null, null, null, null, null, null, null));
		}

	
	      return userList; 
	   }
   public List<Portfolio> getAllPortfolio(String id){ 
	   String connectionUrl = "jdbc:sqlserver://127.0.0.1:1433;" +
				"databaseName=OracleDB;integratedSecurity=true;";
	  java.sql.Connection conn = null;      
	    
	      List<Portfolio> userList =new ArrayList<Portfolio>(); 
	      try {
	    	  Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
		    	conn = DriverManager.getConnection(connectionUrl);
		    	System.out.println("Connected to Sql \n");
	    	  // String sql ="select sysdate as current_day from dual";
			String sql = "select DATASET_HOLDINGS.ISSUER_CODE AS t1,SHORT_NAME as Holdn,sum(CONVERT(numeric,qnty_held)) as Qty from DATASET_HOLDINGS INNER JOIN DATASET_PRICES ON DATASET_PRICES.ISSUER_CODE=DATASET_HOLDINGS.ISSUER_CODE where CLIENT_PREFIX='"+ id +"' group by DATASET_HOLDINGS.ISSUER_CODE,SHORT_NAME,qnty_held";
			//creating PreparedStatement object to execute query
			PreparedStatement preStatement = conn.prepareStatement(sql);
			ResultSet result = preStatement.executeQuery();
			while (result.next()) {
				userList.add(new Portfolio(result.getString("t1"),result.getString("Holdn"), result.getString("Qty") ));
			

			
			} 
		} catch (SQLException | ClassNotFoundException e) {
			// TODO: handle exception
			userList.add(new Portfolio(e.toString(),e.toString(),e.toString()));
		}
	      return userList; 
	   } 

   public List<DATASET_PRICES> getAllDATASET_PRICES(){ 
	   String connectionUrl = "jdbc:sqlserver://127.0.0.1:1433;" +
				"databaseName=OracleDB;integratedSecurity=true;";
	  java.sql.Connection conn = null;      
	    
	      List<DATASET_PRICES> userList =new ArrayList<DATASET_PRICES>(); 
	      try {
	//creating connection to Oracle database using JDBC
	    	  Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
		    	conn = DriverManager.getConnection(connectionUrl);
		    	System.out.println("Connected to Sql \n");
		    	// String sql ="select sysdate as current_day from dual";
			String sql = "select Trade_date,ISSUER_CODE,ISIN, MAIN_TYPE,SUB_TYPE,PRICE_HI,PRICE_LOW,CLOSE_PRICE,AVERAGE_PRICE,SHARE_VOLUME,num_of_trades,TURNOVER from DATASET_PRICES order by Trade_date desc";
			//creating PreparedStatement object to execute query
			PreparedStatement preStatement = conn.prepareStatement(sql);
			ResultSet result = preStatement.executeQuery();
			while (result.next()) {
				userList.add(new DATASET_PRICES(result.getString("Trade_date"),result.getString("ISSUER_CODE"),result.getString("ISIN"), result.getString("MAIN_TYPE"),result.getString("SUB_TYPE"),result.getString("PRICE_HI"),
						result.getString("PRICE_LOW"),result.getString("CLOSE_PRICE"),result.getString("AVERAGE_PRICE"),result.getString("SHARE_VOLUME"),result.getString("num_of_trades"),result.getString("TURNOVER") ));
			

			
			} 
		} catch (SQLException | ClassNotFoundException e) {
			// TODO: handle exception
			userList.add(new DATASET_PRICES(e.toString(),e.toString(),e.toString(),e.toString(),e.toString(),
					e.toString(),e.toString(), e.toString(),e.toString(),e.toString(),e.toString(), null));
		}
	      return userList; 
	   } 
   public List<DATASET_PRICES> getAllDATASET_PRICESG(){ 
	   String connectionUrl = "jdbc:sqlserver://127.0.0.1:1433;" +
				"databaseName=OracleDB;integratedSecurity=true;";
	  java.sql.Connection conn = null;      
	    	      
	      List<DATASET_PRICES> userList =new ArrayList<DATASET_PRICES>(); 
	      try {
	    	  Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
		    	conn = DriverManager.getConnection(connectionUrl);
		    	System.out.println("Connected to Sql \n");
		    	// String sql ="select sysdate as current_day from dual";
			String sql = "select top 20 Trade_date,ISSUER_CODE,ISIN, MAIN_TYPE,SUB_TYPE,PRICE_HI,PRICE_LOW,CLOSE_PRICE,AVERAGE_PRICE,SHARE_VOLUME,num_of_trades,TURNOVER,DECODE(CLOSE_PRICE,0,0,((PRICE_HI - CLOSE_PRICE)/CLOSE_PRICE)*100) As Change from DATASET_PRICES WHERE DECODE(CLOSE_PRICE,0,0,((PRICE_HI - CLOSE_PRICE)/CLOSE_PRICE)*100)>0 order by DECODE(CLOSE_PRICE,0,0,((PRICE_HI - CLOSE_PRICE)/CLOSE_PRICE)*100) desc";
			//creating PreparedStatement object to execute query
			PreparedStatement preStatement = conn.prepareStatement(sql);
			ResultSet result = preStatement.executeQuery();
			while (result.next()) {
				userList.add(new DATASET_PRICES(result.getString("Trade_date"),result.getString("ISSUER_CODE"),result.getString("ISIN"), result.getString("MAIN_TYPE"),result.getString("SUB_TYPE"),result.getString("PRICE_HI"),
						result.getString("PRICE_LOW"),result.getString("CLOSE_PRICE"),result.getString("AVERAGE_PRICE"),result.getString("SHARE_VOLUME"),result.getString("num_of_trades"),result.getString("TURNOVER") ));
			

			
			} 
		} catch (SQLException | ClassNotFoundException e) {
			// TODO: handle exception
			userList.add(new DATASET_PRICES(e.toString(),e.toString(),e.toString(),e.toString(),e.toString(),
					e.toString(),e.toString(), e.toString(),e.toString(),e.toString(),e.toString(), null));
		}
	      return userList; 
	   } 
   public List<DATASET_PRICES> getAllDATASET_PRICESL(){ 
	   String connectionUrl = "jdbc:sqlserver://127.0.0.1:1433;" +
				"databaseName=OracleDB;integratedSecurity=true;";
	  java.sql.Connection conn = null;      
	    	      
	      List<DATASET_PRICES> userList =new ArrayList<DATASET_PRICES>(); 
	      try {
	    	  Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
		    	conn = DriverManager.getConnection(connectionUrl);
		    	System.out.println("Connected to Sql \n");
	    	// String sql ="select sysdate as current_day from dual";
			String sql = "select Top 20 Trade_date,ISSUER_CODE,ISIN, MAIN_TYPE,SUB_TYPE,PRICE_HI,PRICE_LOW,CLOSE_PRICE,AVERAGE_PRICE,SHARE_VOLUME,num_of_trades,TURNOVER,DECODE(CLOSE_PRICE,0,0,((PRICE_HI - CLOSE_PRICE)/CLOSE_PRICE)*100) As Change from DATASET_PRICES WHERE DECODE(CLOSE_PRICE,0,0,((PRICE_HI - CLOSE_PRICE)/CLOSE_PRICE)*100)<0  order by DECODE(CLOSE_PRICE,0,0,((PRICE_HI - CLOSE_PRICE)/CLOSE_PRICE)*100) asc ";
							//creating PreparedStatement object to execute query
			PreparedStatement preStatement = conn.prepareStatement(sql);
			ResultSet result = preStatement.executeQuery();
			while (result.next()) {
				userList.add(new DATASET_PRICES(result.getString("Trade_date"),result.getString("ISSUER_CODE"),result.getString("ISIN"), result.getString("MAIN_TYPE"),result.getString("SUB_TYPE"),result.getString("PRICE_HI"),
						result.getString("PRICE_LOW"),result.getString("CLOSE_PRICE"),result.getString("AVERAGE_PRICE"),result.getString("SHARE_VOLUME"),result.getString("num_of_trades"),result.getString("TURNOVER") ));
			

			
			} 
		} catch (SQLException | ClassNotFoundException e) {
			// TODO: handle exception
			userList.add(new DATASET_PRICES(e.toString(),e.toString(),e.toString(),e.toString(),e.toString(),
					e.toString(),e.toString(), e.toString(),e.toString(),e.toString(),e.toString(), null));
		}
	      return userList; 
	   } 
   public List<ESCROW_CLIENTS> getAllESCROW_CLIENTS(String id){ 
	   String connectionUrl = "jdbc:sqlserver://127.0.0.1:1433;" +
				"databaseName=OracleDB;integratedSecurity=true;";
	  java.sql.Connection conn = null;      
	    
	      List<ESCROW_CLIENTS> userList =new ArrayList<ESCROW_CLIENTS>(); 
	      try {

	    	  Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
		    	conn = DriverManager.getConnection(connectionUrl);
		    	System.out.println("Connected to Sql \n");
		    	// String sql ="select sysdate as current_day from dual";
	String sql = "SELECT top 1 ESCROW_CLIENTS.MEMBER_CODE,ESCROW_CLIENTS.MEMBER_TYPE,ESCROW_CLIENTS.CLIENT_PREFIX, ESCROW_CLIENTS.CLIENT_SUFFIX, ESCROW_CLIENTS.CLIENT_TYPE,ESCROW_CLIENTS.SURNAME,ESCROW_CLIENTS.OTHER_NAMES,ESCROW_CLIENTS.DATE_OF_REGISTRATION,CONVERT(date,ESCROW_CLIENTS.DATE_CHANGED,105) as 'DATE_CHANGED',ESCROW_CLIENTS.TELEPHONE,ESCROW_CLIENTS.E_MAIL,(ESCROW_CLIENTS.ADDRESS1 +  '-' + ESCROW_CLIENTS.ADDRESS2 + '-'+ ESCROW_CLIENTS.ADDRESS3) as ADDRESS,(SELECT BANK_NAME FROM ESCROW_BANK_ACCOUNTS WHERE Client_Prefix='"+ id +"') AS BANK,(SELECT BRANCH_NAME FROM ESCROW_BANK_ACCOUNTS WHERE Client_Prefix='"+ id +"') AS BRANCH,(SELECT ACCOUNT_NO FROM ESCROW_BANK_ACCOUNTS WHERE Client_Prefix='"+ id +"') AS ACCOUNT FROM  ESCROW_CLIENTS inner join ESCROW_AUDIT_CLIENTS ON ESCROW_CLIENTS.CLIENT_PREFIX=ESCROW_AUDIT_CLIENTS.CLIENT_PREFIX where ESCROW_CLIENTS.CLIENT_PREFIX='"+ id +"' order by CONVERT(date,ESCROW_CLIENTS.DATE_CHANGED,105) desc ";
			//creating PreparedStatement object to execute query
			PreparedStatement preStatement = conn.prepareStatement(sql);
			ResultSet result = preStatement.executeQuery();
			while (result.next()) {
				userList.add(new ESCROW_CLIENTS(result.getString("MEMBER_CODE"),result.getString("MEMBER_TYPE"),result.getString("CLIENT_PREFIX"),result.getString("CLIENT_SUFFIX"),result.getString("CLIENT_TYPE"),
						result.getString("SURNAME"),result.getString("OTHER_NAMES"),result.getString("DATE_OF_REGISTRATION"),result.getString("DATE_CHANGED"),result.getString("TELEPHONE"),result.getString("E_MAIL"),result.getString("ADDRESS"),result.getString("BANK"),result.getString("BRANCH"),result.getString("ACCOUNT") ));
			

			
			} 
		} catch (SQLException | ClassNotFoundException e) {
			// TODO: handle exception
			userList.add(new ESCROW_CLIENTS(e.toString(),e.toString(),e.toString(),e.toString(),e.toString(),
					e.toString(),e.toString(), e.toString(),e.toString(),e.toString(),e.toString(), id, id, id, id));
		}
	      return userList; 
	   } 
   public List<ESCROW_CLIENTS> getAllESCROW_CLIENTS2(String id){ 
	   String connectionUrl = "jdbc:sqlserver://127.0.0.1:1433;" +
				"databaseName=OracleDB;integratedSecurity=true;";
	  java.sql.Connection conn = null;      
	    
	      List<ESCROW_CLIENTS> userList =new ArrayList<ESCROW_CLIENTS>(); 
	      try {
	    	  Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
		    	conn = DriverManager.getConnection(connectionUrl);
		    	System.out.println("Connected to Sql \n");
		    	// String sql ="select sysdate as current_day from dual";
	String sql = "SELECT top 1 ESCROW_AUDIT_CLIENTS.MEMBER_CODE,ESCROW_AUDIT_CLIENTS.MEMBER_TYPE,ESCROW_AUDIT_CLIENTS.CLIENT_PREFIX,ESCROW_AUDIT_CLIENTS.CLIENT_SUFFIX, ESCROW_AUDIT_CLIENTS.CLIENT_TYPE,ESCROW_AUDIT_CLIENTS.SURNAME,ESCROW_AUDIT_CLIENTS.OTHER_NAMES,ESCROW_AUDIT_CLIENTS.DATE_OF_REGISTRATION,CONVERT(date,ESCROW_AUDIT_CLIENTS.DATE_CHANGED,105) as 'DATE_CHANGED',ESCROW_AUDIT_CLIENTS.TELEPHONE,ESCROW_AUDIT_CLIENTS.E_MAIL,(ESCROW_AUDIT_CLIENTS.ADDRESS1 +  '-' + ESCROW_AUDIT_CLIENTS.ADDRESS2 + '-'+ ESCROW_AUDIT_CLIENTS.ADDRESS3) as ADDRESS,(SELECT BANK_NAME FROM ESCROW_BANK_ACCOUNTS WHERE Client_Prefix='"+ id +"') AS BANK,(SELECT BRANCH_NAME FROM ESCROW_BANK_ACCOUNTS WHERE Client_Prefix='"+id+"') AS BRANCH,(SELECT ACCOUNT_NO FROM ESCROW_BANK_ACCOUNTS WHERE Client_Prefix='"+id+"') AS ACCOUNT FROM  ESCROW_AUDIT_CLIENTS inner join ESCROW_CLIENTS ON ESCROW_AUDIT_CLIENTS.CLIENT_PREFIX=ESCROW_CLIENTS.CLIENT_PREFIX where ESCROW_AUDIT_CLIENTS.CLIENT_PREFIX='"+id+"' order by CONVERT(date,ESCROW_AUDIT_CLIENTS.DATE_CHANGED,105) desc";
			//creating PreparedStatement object to execute query
			PreparedStatement preStatement = conn.prepareStatement(sql);
			ResultSet result = preStatement.executeQuery();
			while (result.next()) {
				userList.add(new ESCROW_CLIENTS(result.getString("MEMBER_CODE"),result.getString("MEMBER_TYPE"),result.getString("CLIENT_PREFIX"),result.getString("CLIENT_SUFFIX"),result.getString("CLIENT_TYPE"),
						result.getString("SURNAME"),result.getString("OTHER_NAMES"),result.getString("DATE_OF_REGISTRATION"),result.getString("DATE_CHANGED"),result.getString("TELEPHONE"),result.getString("E_MAIL"),result.getString("ADDRESS"),result.getString("BANK"),result.getString("BRANCH"),result.getString("ACCOUNT") ));
			

			
			} 
		} catch (SQLException | ClassNotFoundException e) {
			// TODO: handle exception
			userList.add(new ESCROW_CLIENTS(e.toString(),e.toString(),e.toString(),e.toString(),e.toString(),
					e.toString(),e.toString(), e.toString(),e.toString(),e.toString(),e.toString(), id, id, id, id));
		}
	      return userList; 
	   } 

   
   public String getNumber(String id){ 
	   String connectionUrl = "jdbc:sqlserver://127.0.0.1:1433;" +
				"databaseName=OracleDB;integratedSecurity=true;";
	  java.sql.Connection conn = null;      
	    
	   String tel="";
	     try {
	    	 Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
		    	conn = DriverManager.getConnection(connectionUrl);
		    	System.out.println("Connected to Sql \n");
		    	// String sql ="select sysdate as current_day from dual";
			String sql = "select top 1 TELEPHONE from  ESCROW_CLIENTS where CLIENT_PREFIX='"+ id +"' order by CONVERT(date,DATE_CHANGED,105) desc";
			//creating PreparedStatement object to execute query
			PreparedStatement preStatement = conn.prepareStatement(sql);
			ResultSet result = preStatement.executeQuery();
			
			while (result.next()) {
				tel=result.getString("TELEPHONE");
		
			
			} 
		} catch (SQLException | ClassNotFoundException e) {
			// TODO: handle exception
			}
	     //int i = Integer.parseInt(tel);
	      return tel; 
	   }   
   public String getIssuerPrice(String id){ 
	   String connectionUrl = "jdbc:sqlserver://127.0.0.1:1433;" +
				"databaseName=OracleDB;integratedSecurity=true;";
	  java.sql.Connection conn = null;      
	    
	   String tel="";
	     try {

	    	 Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
		    	conn = DriverManager.getConnection(connectionUrl);
		    	System.out.println("Connected to Sql \n");
		    	// String sql ="select sysdate as current_day from dual";
			String sql = "select top 1 CLOSE_PRICE from DATASET_PRICES where ISSUER_CODE='"+ id +"' order by Trade_date desc ";
			//creating PreparedStatement object to execute query
			PreparedStatement preStatement = conn.prepareStatement(sql);
			ResultSet result = preStatement.executeQuery();
			
			while (result.next()) {
				tel=result.getString("CLOSE_PRICE");
		
			
			} 
		} catch (SQLException | ClassNotFoundException e) {
			// TODO: handle exception
			}
	     int i = Integer.parseInt(tel);
	      return Integer.toString(i); 
	   }   
   
   public String getID(String id){ 
	   String connectionUrl = "jdbc:sqlserver://127.0.0.1:1433;" +
				"databaseName=OracleDB;integratedSecurity=true;";
	  java.sql.Connection conn = null;      
	    
	   String tel="";
	     try {
	    	 Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
		    	conn = DriverManager.getConnection(connectionUrl);
		    	System.out.println("Connected to Sql \n");
		    	// String sql ="select sysdate as current_day from dual";
			String sql = "select top 1 LOCAL_CLIENT_ID from  ESCROW_CLIENTS where CLIENT_PREFIX='"+ id +"' order by CONVERT(date,DATE_CHANGED,105) desc";
			//creating PreparedStatement object to execute query
			PreparedStatement preStatement = conn.prepareStatement(sql);
			ResultSet result = preStatement.executeQuery();
			
			while (result.next()) {
				tel=result.getString("LOCAL_CLIENT_ID");
		
			
			} 
		} catch (SQLException | ClassNotFoundException e) {
			// TODO: handle exception
			}
	    // int i = Integer.parseInt(tel);
	      return tel; 
	   } 
   public String getEmail(String id){ 
	   String connectionUrl = "jdbc:sqlserver://127.0.0.1:1433;" +
				"databaseName=OracleDB;integratedSecurity=true;";
	  java.sql.Connection conn = null;      
	    
	   String tel="";
	     try {

	    	 Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
		    	conn = DriverManager.getConnection(connectionUrl);
		    	System.out.println("Connected to Sql \n");// String sql ="select sysdate as current_day from dual";
			String sql = "select top 1 E_MAIL from  ESCROW_CLIENTS where CLIENT_PREFIX='"+ id +"' order by CONVERT(date,DATE_CHANGED,105) desc";
			//creating PreparedStatement object to execute query
			PreparedStatement preStatement = conn.prepareStatement(sql);
			ResultSet result = preStatement.executeQuery();
			
			while (result.next()) {
				tel=result.getString("E_MAIL");
		
			
			} 
		} catch (SQLException | ClassNotFoundException e) {
			// TODO: handle exception
			tel=e.getMessage();
			}

	      return tel; 
	   } 
      
   public List<ESCROW_MINI_STMT> getAllESCROW_MINI_STMT(String id,String dt1,String dt2){ 
	   String connectionUrl = "jdbc:sqlserver://127.0.0.1:1433;" +
				"databaseName=OracleDB;integratedSecurity=true;";
	  java.sql.Connection conn = null;      
	    
	      List<ESCROW_MINI_STMT> userList =new ArrayList<ESCROW_MINI_STMT>(); 
	      try {

	    	  Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
		    	conn = DriverManager.getConnection(connectionUrl);
		    	System.out.println("Connected to Sql \n");// String sql ="select sysdate as current_day from dual";
			String sql = "SELECT CLIENT_PREFIX, MEMBER_CODE,  ISSUER_CODE, MAIN_TYPE, description,Convert(date,date_system,105) as date_system, tshares from ESCROW_MINI_STMT where CLIENT_PREFIX ='"+ id +"' and Convert(date,date_system,105)>=CAST('"+ dt1 +"' as date) and Convert(date,date_system,105)<=CAST('"+ dt2 +"' as date) order by Convert(date,date_system,105) desc";
			//creating PreparedStatement object to execute query
			PreparedStatement preStatement = conn.prepareStatement(sql);
			ResultSet result = preStatement.executeQuery();
			while (result.next()) {
				userList.add(new ESCROW_MINI_STMT(result.getString("CLIENT_PREFIX"),result.getString("MEMBER_CODE"),result.getString("ISSUER_CODE"),result.getString("MAIN_TYPE"),result.getString("description"),result.getString("date_system"),result.getString("tshares")));
			

			
			} 
		} catch (SQLException | ClassNotFoundException e) {
			// TODO: handle exception
			userList.add(new ESCROW_MINI_STMT(e.toString(),e.toString(),e.toString(),e.toString(),e.toString(),
					e.toString(),e.toString()));
		}
	      return userList; 
	   } 
   public List<ALERTS> getAllALERTS(String id){ 
	   String connectionUrl = "jdbc:sqlserver://127.0.0.1:1433;" +
				"databaseName=OracleDB;integratedSecurity=true;";
	  java.sql.Connection conn = null;      
	    
	      List<ALERTS> userList =new ArrayList<ALERTS>(); 
	      try {

	    	  Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
		    	conn = DriverManager.getConnection(connectionUrl);
		    	System.out.println("Connected to Sql \n");// String sql ="select sysdate as current_day from dual";
			String sql = "select top 1 * from ESCROW_OTHER_ALERTS where CLIENT_PREFIX='"+ id +"' order by CAST(DATE_APPLICATION AS date) desc";
			//creating PreparedStatement object to execute query
			PreparedStatement preStatement = conn.prepareStatement(sql);
			ResultSet result = preStatement.executeQuery();
			while (result.next()) {
				userList.add(new ALERTS(result.getString("CLIENT_PREFIX"),result.getString("CLIENT_SUFFIX"),result.getString("ISSUER_CODE"),result.getString("MEMBER_CODE"),
						result.getString("DESCRIPTION"),result.getString("DATE_APPLICATION"),result.getString("TRANSACTION_QTY"),result.getString("TRANSACTION_REFERENCE")));
			

			
			} 
		} catch (SQLException | ClassNotFoundException e) {
			// TODO: handle exception
			userList.add(new ALERTS(e.toString(),e.toString(),e.toString(),e.toString(),e.toString(),
					e.toString(),e.toString(),e.toString()));
		}
	      return userList; 
	   } 

   public List<TRADES> getAllTRADES(String id){ 
	   String connectionUrl = "jdbc:sqlserver://127.0.0.1:1433;" +
				"databaseName=OracleDB;integratedSecurity=true;";
	  java.sql.Connection conn = null;      
	    
	      List<TRADES> userList =new ArrayList<TRADES>(); 
	      try {
	    	  Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
		    	conn = DriverManager.getConnection(connectionUrl);
		    	System.out.println("Connected to Sql \n");// String sql ="select sysdate as current_day from dual";
			String sql = "select top 5 * from ESCROW_TRADES where CLIENT_PREFIX='"+ id +"' order by CAST(DATE_TRADE as date) desc";
			//creating PreparedStatement object to execute query
			PreparedStatement preStatement = conn.prepareStatement(sql);
			ResultSet result = preStatement.executeQuery();
			while (result.next()) {
				userList.add(new TRADES(result.getString("ISSUER_CODE"),result.getString("MAIN_TYPE"),result.getString("SUB_TYPE"),result.getString("QTY"),result.getString("PRICE"),result.getString("DATE_TRADE"),
						result.getString("NET_BUY_SELL"),result.getString("MEMBER_CODE"),result.getString("CLIENT_PREFIX"),result.getString("CLIENT_SUFFIX"),result.getString("CDS_REFERENCE")));
			

			
			} 
		} catch (SQLException | ClassNotFoundException e) {
			// TODO: handle exception
			userList.add(new TRADES(e.toString(),e.toString(),e.toString(),e.toString(),e.toString(),
					e.toString(),e.toString(),e.toString(),e.toString(),e.toString()));
		}
	      return userList; 
	   } 
   
   
   public String getNames(String id){ 
	   String connectionUrl = "jdbc:sqlserver://127.0.0.1:1433;" +
				"databaseName=OracleDB;integratedSecurity=true;";
	  java.sql.Connection conn = null;      
	    
	   String tel="";
	     try {

	    	 Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
		    	conn = DriverManager.getConnection(connectionUrl);
		    	System.out.println("Connected to Sql \n");// String sql ="select sysdate as current_day from dual";
			String sql = "select top 1 * from  ESCROW_CLIENTS where CLIENT_PREFIX='"+ id +"' order by CONVERT(date,DATE_CHANGED,105) desc";
			//creating PreparedStatement object to execute query
			PreparedStatement preStatement = conn.prepareStatement(sql);
			ResultSet result = preStatement.executeQuery();
			
			while (result.next()) {
				tel=result.getString("SURNAME")+" "+result.getString("OTHER_NAMES");
		
			
			} 
		} catch (SQLException | ClassNotFoundException e) {
			// TODO: handle exception
			}
	      return tel; 
	   }   
   public List<IssuerList> getIssuerss(){ 
	   String connectionUrl = "jdbc:sqlserver://127.0.0.1:1433;" +
				"databaseName=OracleDB;integratedSecurity=true;";
	  java.sql.Connection conn = null;      
	    
	   String tel="";
	   List<IssuerList> test  =new ArrayList<IssuerList>(); 
	    
	     try {
		
	    	 Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
		    	conn = DriverManager.getConnection(connectionUrl);
		    	System.out.println("Connected to Sql \n");
		    	// String sql ="select sysdate as current_day from dual";
			String sql = "SELECT DISTINCT(NAME),ISSUER_CODE FROM DATASET_ISSUERS";
			//creating PreparedStatement object to execute query
			PreparedStatement preStatement = conn.prepareStatement(sql);
			ResultSet result = preStatement.executeQuery();
			
			while (result.next()) {
			
		
			test.add(new IssuerList(result.getString("NAME"),result.getString("ISSUER_CODE")));
			} 
		} catch (SQLException | ClassNotFoundException e) {
			// TODO: handle exception
			test.add(new IssuerList(e.toString(),e.toString()));
			
			}
	     
	      return test; 
	   }   
   
   public List<HOLDINGS> HDS(String s){ 
	   String connectionUrl = "jdbc:sqlserver://127.0.0.1:1433;" +
				"databaseName=OracleDB;integratedSecurity=true;";
	  java.sql.Connection conn = null;      
	    
	      List<HOLDINGS> userList =new ArrayList<HOLDINGS>(); 
	      try {
	    	  Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
		    	conn = DriverManager.getConnection(connectionUrl);
		    	System.out.println("Connected to Sql \n");
		    	// String sql ="select sysdate as current_day from dual";
			String sql ="SELECT MEMBER_CODE,SHORT_NAME,ISSUER_CODE,MAIN_TYPE,SUB_TYPE,QTY,ISSUER_NAME FROM ESCROW_PB WHERE CLIENT_PREFIX='"+s+"' ORDER BY ISSUER_CODE";
			//creating PreparedStatement object to execute query
			PreparedStatement preStatement = conn.prepareStatement(sql);
			ResultSet result = preStatement.executeQuery();
			while (result.next()) {
				userList.add(new HOLDINGS(result.getString("MEMBER_CODE"),result.getString("ISSUER_CODE"),result.getString("SHORT_NAME"),result.getString("MAIN_TYPE"),result.getString("SUB_TYPE"),result.getString("QTY"),result.getString("ISSUER_NAME")));
					
			} 
		} catch (SQLException | ClassNotFoundException e) {
			// TODO: handle exception
			userList.add(new HOLDINGS(e.toString(),e.toString(),e.toString(),e.toString(),e.toString(),e.toString(),e.toString()));
		}
	      return userList; 
	   } 
   
   public List<MEMBERS> MEM(String s){ 
	   String connectionUrl = "jdbc:sqlserver://127.0.0.1:1433;" +
				"databaseName=OracleDB;integratedSecurity=true;";
	  java.sql.Connection conn = null;      
	    	      
	      List<MEMBERS> userList =new ArrayList<MEMBERS>(); 
	      try {
	    	  Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
		    	conn = DriverManager.getConnection(connectionUrl);
		    	System.out.println("Connected to Sql \n");// String sql ="select sysdate as current_day from dual";
			String sql ="SELECT * FROM ESCROW_MEMBERS";
			//creating PreparedStatement object to execute query
			PreparedStatement preStatement = conn.prepareStatement(sql);
			ResultSet result = preStatement.executeQuery();
			while (result.next()) {
				userList.add(new MEMBERS(result.getString("MEMBER_CODE"),result.getString("MEMBER_TYPE"),result.getString("NAME")));
					
			} 
		} catch (SQLException | ClassNotFoundException e) {
			// TODO: handle exception
			userList.add(new MEMBERS(e.toString(),e.toString(),e.toString()));
		}
	      return userList; 
	   }
   
   public List<BANK> BAN(String s){ 
	   String connectionUrl = "jdbc:sqlserver://127.0.0.1:1433;" +
				"databaseName=OracleDB;integratedSecurity=true;";
	  java.sql.Connection conn = null;      
	    	      
	      List<BANK> userList =new ArrayList<BANK>(); 
	      try {

	    	  Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
		    	conn = DriverManager.getConnection(connectionUrl);
		    	System.out.println("Connected to Sql \n");
		    	// String sql ="select sysdate as current_day from dual";
			String sql ="SELECT * FROM ESCROW_BANKS";
			//creating PreparedStatement object to execute query
			PreparedStatement preStatement = conn.prepareStatement(sql);
			ResultSet result = preStatement.executeQuery();
			while (result.next()) {
				userList.add(new BANK(result.getString("BANK_CODE"),result.getString("BANK_NAME"),result.getString("BRANCH_CODE"),result.getString("BRANCH_NAME")));
					
			} 
		} catch (SQLException | ClassNotFoundException e) {
			// TODO: handle exception
			userList.add(new BANK(e.toString(),e.toString(),e.toString(), s.toString()));
		}
	      return userList; 
	   }
}
