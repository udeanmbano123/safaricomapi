package com.service;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
@XmlRootElement(name = "ALERTS") 
public class ALERTS implements Serializable {  
	   private static final long serialVersionUID = 1L;
   public ALERTS(){};
	private String TRANSACTION_REFERENCE;
	public String getTRANSACTION_REFERENCE() {
		return TRANSACTION_REFERENCE;
	}
	public void setTRANSACTION_REFERENCE(String tRANSACTION_REFERENCE) {
		TRANSACTION_REFERENCE = tRANSACTION_REFERENCE;
	}
	public String getCLIENT_PREFIX() {
		return CLIENT_PREFIX;
	}
	public void setCLIENT_PREFIX(String cLIENT_PREFIX) {
		CLIENT_PREFIX = cLIENT_PREFIX;
	}
	public ALERTS(String cLIENT_PREFIX, String cLIENT_SUFFIX, String iSSUER_CODE,
			String mEMBER_CODE, String dESCRIPTION, String dATE_APPLICATION, String tRANSACTION_QTY,String tRANSACTION_REFERENCE) {
		super();
		TRANSACTION_REFERENCE = tRANSACTION_REFERENCE;
		CLIENT_PREFIX = cLIENT_PREFIX;
		CLIENT_SUFFIX = cLIENT_SUFFIX;
		ISSUER_CODE = iSSUER_CODE;
		MEMBER_CODE = mEMBER_CODE;
		DESCRIPTION = dESCRIPTION;
		DATE_APPLICATION = dATE_APPLICATION;
		TRANSACTION_QTY = tRANSACTION_QTY;
	}
	public String getCLIENT_SUFFIX() {
		return CLIENT_SUFFIX;
	}
	public void setCLIENT_SUFFIX(String cLIENT_SUFFIX) {
		CLIENT_SUFFIX = cLIENT_SUFFIX;
	}
	public String getISSUER_CODE() {
		return ISSUER_CODE;
	}
	public void setISSUER_CODE(String iSSUER_CODE) {
		ISSUER_CODE = iSSUER_CODE;
	}
	public String getMEMBER_CODE() {
		return MEMBER_CODE;
	}
	public void setMEMBER_CODE(String mEMBER_CODE) {
		MEMBER_CODE = mEMBER_CODE;
	}
	public String getDESCRIPTION() {
		return DESCRIPTION;
	}
	public void setDESCRIPTION(String dESCRIPTION) {
		DESCRIPTION = dESCRIPTION;
	}
	public String getDATE_APPLICATION() {
		return DATE_APPLICATION;
	}
	public void setDATE_APPLICATION(String dATE_APPLICATION) {
		DATE_APPLICATION = dATE_APPLICATION;
	}
	public String getTRANSACTION_QTY() {
		return TRANSACTION_QTY;
	}
	public void setTRANSACTION_QTY(String tRANSACTION_QTY) {
		TRANSACTION_QTY = tRANSACTION_QTY;
	}
	private String CLIENT_PREFIX; 
	private String CLIENT_SUFFIX;
	private String  ISSUER_CODE;
	private String MEMBER_CODE; 
	private String DESCRIPTION; 
	private String DATE_APPLICATION; 
	private String TRANSACTION_QTY;

	
}
